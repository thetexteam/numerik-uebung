function [] = hausaufgabe3()

left = 0;                                       % linke Intervallgrenze
right = 1;                                      % rechte Intervallgrenze
n = 100;                                        % Anzahl der inneren Stützstellen

figure;
subplot(1, 2, 1);
plot(1,1);
title('berechnete L�sung');
xlabel('x');
ylabel('u');
subplot(1, 2, 2);
x = (left:0.01:right);
plot(x, -0.5*x.^2 + 0.5*x);
title('exakte L�sung');
xlabel('x');
ylabel('u');


plotSOR;
plotPCG;

end

function [x,iter] = sor(A,b,x0,omega, TOL)
   D = diag(diag(A));
   L = tril(A,-1);
   R = triu(A,1);
   M = 1/omega*D + L;
   N = (1/omega - 1)*D - R;
   
   iter = 0;
   x = x0;
   xnext = x + 10*TOL;
   while norm(xnext - x) > TOL
       xnext = M\(N*x + b);
       iter = iter + 1;
       x = xnext;
   end
   
end

function [x,iter] = pcg(A,b,P,x0,TOL)
   
end

function [A,b] = finiteDifferenzen(left,right,n)
    
end

function f = rightHandSide(x)
    
    
end

function plotSOR()
    figure;                                             % erstelle neues Bild
    left = 0;                                           % linke Intervallgrenze
    right = 1;                                          % rechte Intervallgrenze
    
    omega = [1, 1.5, 1.9];                              % verschiedene Relaxationsparameter
   
    % insert code and plot here
    
    disp('Erläuterung zum optimalen Relaxationsparameter: ')
end

function plotPCG()
    figure;                                                                                     % neues Bild 
    left = 0;                                                                                   % linke Intervallgrenze
    right = 1;                                                                                  % rechte Intervallgrenze
    
    % insert code and plot here
    
    display('Erläuterung zu den Vorkonditionierern: ');
end
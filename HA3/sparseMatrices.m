function [] = sparseMatrices()
    iterEnd = 1000;                                                             % largest size of A
    iterStart = 10;                                                             % smallest size of A
    step = 10;                                                                  % step size
    
    iter = 1;
    p = iterStart : step : iterEnd;
    
    memoryFull = zeros(1,length(p));
    timeFull = zeros(1,length(p));
    memorySparse = zeros(1,length(p));
    timeSparse = zeros(1,length(p));
    memoryCOO = zeros(1,length(p));
    timeCOO = zeros(1,length(p));
    
    for i=p
        
        [AFull, ASparse, row, col, val] = createSparseAndFull(i,i,0.1*i);
        
        b = ones(i,1);
        memoryFull(iter) = getMemMatrix(AFull);
        timeFull(iter) = multiplyMatrix(AFull,b);
        memorySparse(iter) = getMemMatrix(ASparse);
        timeSparse(iter) = multiplyMatrix(ASparse,b);
        memoryCOO(iter) = getMemCOO(row, col, val);
        timeCOO(iter) = multiplyCOO(row, col, val, b);
        
        iter = iter+1;                                                          % start next iteration
    end
    
    figure;
    subplot(1, 2, 1);
    hold on;
    plot(p, memoryFull,'DisplayName','AFull');
    plot(p, memorySparse,'r','DisplayName','ASparse');
    plot(p, memoryCOO,'g','DisplayName','COO');
    title('Speicherbedarf');
    set(gca,'YScale','log');
    xlabel('n');
    ylabel('memory');
	legend('show','Location','se');
    
    subplot(1, 2, 2);
    hold on;
    plot(p, timeFull,'DisplayName','AFull');
    plot(p, timeSparse,'r','DisplayName','ASparse');
    plot(p, timeCOO,'g','DisplayName','COO');
    title('Zeit');
    set(gca,'YScale','log');
    xlabel('n');
    ylabel('time');
    legend('show','Location','se');

    display('Erläuterungen zur Aufgabe 1: ');
    
end

function [AFull, ASparse, row, col, val] = createSparseAndFull(m,n, nonzeros)
   
AFull = zeros(m,n);
zufallsvektor = randi(numel(AFull),nonzeros,1);
randomMatrix = rand(m,n);

AFull(zufallsvektor) = randomMatrix(zufallsvektor);
ASparse = sparse(AFull);
[row,col,val] = find(ASparse);

end

function [mem] = getMemMatrix(A)
[~] = A;

var = whos('A');
mem = var.bytes;

end

function [mem] = getMemCOO(row, col, val)
[~] = row + col + val;

var = whos('row');
mem = var.bytes;
var = whos('col');
mem = mem + var.bytes;
var = whos('val');
mem = mem + var.bytes;

end

function [time] = multiplyMatrix(A,b)
  
tic;
[~] = A * b;
time = toc;

end


function [time] = multiplyCOO(row, col, val, b)
  
tic;
x = zeros(length(b),1);
for i = 1:length(row)
    x(row(i)) = x(row(i)) + val(i) * b(col(i));
end
time = toc;
end
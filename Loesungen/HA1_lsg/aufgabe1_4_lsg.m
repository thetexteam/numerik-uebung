% Aufgabe 4: Die Reihenfolge von Operationen kann das Ergebnis beeinflussen
%
% In der Numerik ist es manchmal vorteilhaft, eine Formel in einen mathematisch äquivalenten Ausdruck umzuformen.
% Dies soll an folgendem Beispiel demonstriert werden.
%
% Die Summe über k=1,2...K von k^-2 konvergiert für K->inf zu pi²/6.
% 
% Berechnen Sie die Summe mit einfacher Genauigkeit (single precision) bis K=10^8. Brechen Sie die Schleife ab, wenn sich die Summe nicht mehr
% verändert und geben Sie das k zu diesem Zeitpunkt aus.
% Speichern Sie die Abweichung zu pi²/6 in 'err_forward'.
%
% Führen Sie nun die Summation in umgekehrter Reihenfolge (k=K,K-1,...1) durch und speichern Sie die Abweichung zu pi²/6 in 'err_backward'.
%
% Vergleichen und begründen Sie die Ergebnisse.

function aufgabe1_4_lsg
K=10^8;

sum1 = single(0);
sum2 = single(0);

for n=1:K
    k=single(n);    
    sum1_old = sum1;
    sum1 = sum1 + single(1)/k^single(2);
    if(sum1 == sum1_old)
        disp('Keine Änderung mehr ab')
        disp(k)
        break;
    end
end

err_forward = abs(pi*pi/6 - sum1);

for n=K:-1:1
    k=single(n); 
   sum2 = sum2 + single(1)/k^single(2);
end

err_backward = abs(pi*pi/6 - sum2);

display(err_forward)
display(err_backward)
disp('Erläuterung')

end
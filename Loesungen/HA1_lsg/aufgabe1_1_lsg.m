% Aufgabe 1: Numerische Ph??nomene
% 
% Vervollst??ndigen Sie die Funktion rundungsfehler():
% Eingabewerte sind eine Zahl 'x' sowie die Anzahl der Iterationen 'n'
% R??ckgabewert ist 'f'
% Die Funktion besteht aus zwei Schleifen. In der ersten Schleife soll n-mal die Wurzel aus x gezogen, in der zweiten Schleife x wieder n-mal quadriert werden.
%
% Vervollst??ndigen Sie das Hauptprogramm:
% Werten Sie die Funktion mit x=100 und n=60 aus und speichern Sie das Ergebnis in der Variable 'result', die vom Hauptprogramm ausgegeben wird.
% Beschreiben und erkl??ren Sie Ihre Beobachtungen.
%
% Ermitteln Sie den R??ckgabewert der Funktion ??ber n von 1 bis 60, speichern Sie die Werte im Feld 'results' und stellen Sie sie graphisch dar.
% Skalieren Sie die Achsen sinnvoll und beschriften Sie diese.
% 
% N??tzliche Befehle: sqrt

function aufgabe1_1_lsg
N=60;
x=100;

result=0;

result=rundungsfehler(x,N);

display(result)
str=['Ergebnis f?r n = ',num2str(N),' und x = ', num2str(x),' ist ',num2str(result)];
disp(str);

results=zeros(N,1);

for n=1:N
    results(n)=rundungsfehler(x,n);
end

plot(results);
axis([1 N 0 x+1])
title('Visualisierung zu Aufgabe 1 _ 1')
xlabel('Iterationen')
ylabel('R?ckgabewert')

end


function f = rundungsfehler(x,n)
for i=1:n
    x = sqrt(x)
end;
disp('first loop done')
for i=1:n
    x = x*x
end;

f = x;
end
% Aufgabe 5: Power-Iteration
%
% Die Power-Iteration-Methode zur numerischen Bestimmung des gr????ten Eigenwerts und des dazugeh??rigen Eigenvektors einer Matrix M ist wie folgt definiert:
%
% x := Mx
% x := x/||x||
% bis x konvergiert
%
% Wenn M genau einen betragsm????ig h??chsten Eigenwert besitzt, dann konvergiert x zu einem Vielfachen des korrespondierenden Eigenvektors.
%
%
% ---------------------------------------------------------------------------
% Aufgabenteil I
% 
% In diesem Aufgabenteil soll die Methode verstanden und veranschaulicht werden.
% 
% Gegeben ist die Matrix A= [0.4 -0.6 0.2; -0.3 0.7 -0.4; -0.1 -0.4 0.5]
%
% a) F??hren Sie auf dem Papier Gau??-Elimination an der Matrix A aus. Welchen Rang hat die Matrix A? Was bedeutet dies f??r die Eigenwerte?
% Berechnen Sie die Eigenwerte und die dazugeh??rigen Eigenvektoren mit der eig-Funktion in MATLAB. Welches ist der gr????te Eigenwert lambda_max und zu 
% welchem Eigenvektor v_max geh??rt er?
%
% Implementieren Sie die oben vorgestellte Methode in der vorgegebenen Funktion showPowerIteration():
% Eingabeparameter: Matrix 'M', Startvektor 'x', Anzahl der Iterationen 'N'
%
% Berechnen Sie f??r jede Iteration den gesch??tzten gr????ten Eigenwert von A.
% Ermitteln Sie in jedem Iterationsschritt die Differenz vom normierten 'x' zum tats??chlichen 'v_max'. Als Fehlerma?? dient der Euklidsche Abstand.
% Stellen Sie diese Differenz sowie den ermittelten gr????ten Eigenwert f??r n=1,2...N graphisch nebeneinander (subplot) dar. Skalieren Sie den Betrag
% der Differenz logarithmisch (log10), w??hlen Sie eine geeignete Achsenskalierung und beschriften Sie die Zeichnung vollst??ndig.
%
% Vervollst??ndigen Sie das Hauptprogramm:
%
% b) Rufen Sie die Funktion im Hauptprogramm f??r M=A, N=50 und 100 zuf??llig generierte x auf. 
% ??berlagern Sie die Darstellung f??r alle x und beschreiben Sie das Ergebnis.
%
% c) Rufen Sie die Funktion im Hauptprogramm f??r M=A, N=10 und x=v_max auf. 
% Was geschieht im Gegensatz zu a) und warum?
%
% d) Rufen Sie die Funktion im Hauptprogramm f??r M=A, N=10 und x=v_2 auf, dem Eigenvektor zum betragsm????ig zweitgr????ten Eigenwert. 
% Was geschieht im Gegensatz zu a) und b) und warum?
%
% e) Rufen Sie die Funktion im Hauptprogramm f??r M=A, N=50 und x= v_min = 1/sqrt(3) * [1;1;1], den dritten Eigenvektor, auf. 
% Warum geschieht hier etwas unterschiedliches als in d)?
%
% ---------------------------------------------------------------------------
% Aufgabenteil II
%
% In diesem Aufgabenteil soll die Rechenzeit der Power-Iteration-Methode untersucht werden.
%
% Hierzu ist eine Modifikation der bisher implementierten Funktion n??tig. Um die Funktionalit??t von Aufgabenteil I nicht zu beeintr??chtigen,
% implementieren Sie f??r Aufgabenteil II die Funktion measurePowerIteration():
% Eingabeparameter: Wie showPowerIteration(), au??erdem Abbruchparameter 'epsilon'
% R??ckgabewert: Rechenzeit 'time'
%
% Berechnen Sie f??r jede Iteration den gesch??tzten gr????ten Eigenwert von M.
% Ermitteln Sie in jedem Iterationsschritt die Differenz vom normierten 'x' zum tats??chlichen 'v_max'.
% Brechen Sie die Methode ab, sobald diese Differenz kleiner als 'epsilon' wird.
% Bestimmen Sie die ben??tigte Rechenzeit sowie die Differenz f??r alle durchlaufenen n und stellen Sie den Zusammenhang graphisch dar. Skalieren Sie die
% Differenz logarithmisch (log10) und beschriften Sie die Zeichnung vollst??ndig.
%
% f) Bestimmen Sie, wie lange die eig-Funktion f??r die Bestimmung von v_max von A ben??tigt und speichern Sie den Wert in 'eig_time_A'.
% Rufen Sie nun die Funktion showPowerIteration() f??r 'A', einen zuf??lligen Vektor 'x', N=1000 sowie epsilon = 10^-12 auf. Speichern Sie die ben??tigte 
% Rechenzeit in 'pi_time_A', vergleichen und kommentieren Sie die Ergebnisse.
%
% g) Die Matrix B sei eine quadratische Zufallsmatrix der Dimension 1000. Berechnen Sie wie in f) die Rechenzeiten f??r beide Methoden und
% speichern Sie die Ergebnisse in 'eig_time_B' sowie 'pi_time_B'. Was beobachten Sie?
%
% h) Nennen Sie einige m??gliche Nachteile bzw. Schwachstellen der Power-Iteration.


function aufgabe1_5_lsg

% Aufgabenteil I
A= [0.4 -0.6 0.2; -0.3 0.7 -0.4; -0.1 -0.4 0.5];
v_max = zeros(3,1);
v_2 = zeros(3,1);
v_min = 1/sqrt(3)*[1;1;1];

% a)
display('Erl??uterung zur Gau??-Elimination an A:')

[V,D]=eig(A)
lambda_max=max(max(D));
v_max = V(:,3);
v_2 = V(:,2);

display(lambda_max)
display(v_max)

% b)
N=50;

figure
subplot(1,2,1)
hold on
subplot(1,2,2)
hold on

for i=1:100
    showPowerIteration(A,rand(3,1),N,v_max);
end

subplot(1,2,1)
hold off
title('Aufgabe b)')
xlabel('Iteration')
ylabel('Fehler ||x-v_{max}||_2');
subplot(1,2,2)
xlabel('Iteration')
ylabel('gr????ter berechneter Eigenwert')
hold off

display('Erl??uterung zu b):')


% c)
N=10;

figure

showPowerIteration(A,v_max,N,v_max);
title('Aufgabe c)')


display('Erl??uterung zu c):')

% d)
N=10;

figure

showPowerIteration(A,v_2,N,v_max);
title('Aufgabe d)')

display('Erl??uterung zu d):')


% e)
N=50;

figure

showPowerIteration(A,v_min,N,v_max);
title('Aufgabe e)')

display('Erl??uterung zu e):')


% Aufgabenteil II
N=1000;
epsilon=10^-12;
eig_time_A=0;
pi_time_A=0;
eig_time_B=0;
pi_time_B=0;

% f)
tic
[V,D]=eig(A);
lambdamax=max(max(D));
[~,col]=find(D==lambdamax);
v_max=V(:,col);
eig_time_A = toc;

figure
xstart = rand(3,1);
pi_time_A = measurePowerIteration(A,xstart,N,v_max,epsilon);
title('Aufgabe f)')

display(eig_time_A)
display(pi_time_A)
display('Erl??uterung zu f):')

% g)
B=rand(1000);

tic
[V,D]=eig(B);
lambdamax=max(max(D));
[~,col]=find(D==lambdamax);
v_max=V(:,col);
eig_time_B = toc;

figure
pi_time_B = measurePowerIteration(B,rand(1000,1),N,v_max,epsilon);
title('Aufgabe g)')

display(eig_time_B)
display(pi_time_B)
display('Erl??uterung zu g):')

% h)
display('Nachteile der Power-Iteration:')

end


function showPowerIteration(M,x,N,v_max)
lambda=zeros(N,1);
diff_vec=zeros(N,1);

for n=1:N
    x_old=x;
    
    x=M*x;
    lambda(n)=x_old'*x;
    x=x./norm(x);
    
    test=norm(v_max-x);
    if(test>=sqrt(2))
        test=norm(v_max+x);
    end
    diff_vec(n)=test;
end

subplot(1,2,1)
plot(log10(diff_vec))
axis([1 N -16 2])
xlabel('Iteration')
ylabel('Fehler')
ylabel('Fehler ||x-v_{max}||_2');

subplot(1,2,2)
plot(lambda,'r')
axis([1 N -1 2])
xlabel('Iteration')
ylabel('Fehler ||x-v_{max}||_2');

end


function time = measurePowerIteration(M,x,N,v_max,epsilon)
tic

for n=1:N
    x=M*x;
    x=x./norm(x);
    
    diff=norm(v_max-x);
    if (diff >= sqrt(2))
        diff = norm(v_max+x);
    end
    diff_vec(n)=diff;
    if norm(diff) <= epsilon
        time_vec(n)=toc;
        break;
    end
    time_vec(n)=toc;
end
plot(time_vec,log10(diff_vec));
xlabel('Zeit t')
ylabel('Fehler ||x-v_{max}||_2')

time=time_vec(n);
end

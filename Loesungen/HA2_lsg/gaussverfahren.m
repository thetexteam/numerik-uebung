function x = gaussverfahren(A,b)

    [m,n] = size(A);                            % Größe der Matrix bestimmen
    if (m ~= n)                                 % Test, ob Matrix quadratisch ist
        disp('Error: Matrix is not square')
    end
    if (n ~= size(b))
        disp('Error: Matrix dimensions do not agree')
    end
    
    tic;                                        % Starte Zeitmessung 
    
    [L,U,P,flopsLU] = luzerlegung(A);           % LU-Zerlegung
    
    time = toc;                                 % Beende Zeitmessung
    ausgabe = ['Die eigene Implementierung benötigt: ' , num2str(time), ' Sekunden.'];
    disp(ausgabe);                              % gib benötigte Zeit aus
    
    tic;                                        % Starte Zeitmessung
    [L1,U1,P1] = lu(A);                         % vorimplementierte LU-Zerlegung
    time = toc;                                 % beende zeitmessung
    ausgabe = ['Die vorimplementierung LU-Zerlegung benötigt: ' , num2str(time), ' Sekunden.'];
    disp(ausgabe);                              % gib benötigte zeit aus
    
    [y,flopsV] = vorwaertssubstitution(L,P,b);  % Vorwärtssubstitution
    [x,flopsR] = rueckwaertssubstitution(U,y);  % Rückwärtssubstitution
    
    ausgabeLU = ['Die LU-Zerlegung benötigt  ',num2str(flopsLU),' flops'];
    ausgabeV = ['Die Vorwärtssub. benötigt   ',num2str(flopsV),' flops'];
    ausgabeR = ['Die Rückwärtssub. benötigt  ',num2str(flopsR),' flops'];    
    ausgabeI = ['Insgesamt benötigt man      ',num2str(flopsLU+flopsV+flopsR),' flops'];
    disp(ausgabeLU);
    disp(ausgabeV);
    disp(ausgabeR);
    disp(ausgabeI);
    
    x_test = A\b;
    if (x~=x_test)
        disp ('Fehler in der Lösung: norm(r) = ');
        disp(norm(x-x_test));
        disp(x);
        disp(x_test);
    end;
end

function [L,U,P,flops] = luzerlegung(A)

    flops = 0;
    [~,n] = size(A);                                % Größe der Matrix
    U = A;                                          % U mit A initialisieren
    L = eye(n);                                     % L mit Einheitsmatrix initialisieren
    P = eye(n);                                     % P mit Einheitsmatrix initialisieren
    for i=1:n-1                                     % alle Zeilen
        pivot = U(i,i);                             % aktuelles Pivotelement
        pivotZeile = i;                             % aktuelle Pivotzeile
        for j=i+1:n                                 % suche in allen nachfolgenden Zeilen
           if (abs(pivot) < abs(U(j,i)));           % wenn aktuelles Element größer als Pivotelement
               pivot = U(j,i);                      % neues Pivotelement gefunden
               pivotZeile = j;                      % neue Pivotzeile gefunden
           end
        end
        
        temp = U(i,:);                              % Zeilentausch in U
        U(i,:) = U(pivotZeile, :);
        U(pivotZeile,:) = temp;
        
        temp = P(i,:);                              % Zeilentausch in P
        P(i,:) = P(pivotZeile, :);
        P(pivotZeile,:) = temp;                 
          
        temp = L(i,1:i-1);                              % Zeilentausch in L
        L(i,1:i-1) = L(pivotZeile, 1:i-1);
        L(pivotZeile,1:i-1) = temp; 
        
        for j=i+1:n                                 % alle darunterliegenden Zeilen
            L(j,i) = U(j,i)/U(i,i);                 % Werte in L bestimmen
            flops = flops + 1;                      % eine Division
            U(j,i) = 0;
            for k=i+1:n                             % alle Spalten rechts von i
                U(j,k) = U(j,k) - L(j,i) * U(i,k);  % Werte in U bestimmen
                flops = flops + 2;                  % eine Subtraktion und eine Multiplikation
            end
        end
    end
end

function [y,flops] = vorwaertssubstitution(L,P,b)
    
    flops = 0;
    [~,n] = size(L);                        % Größe von L
    y = P*b;                                % Ergebnisvektor mit rechter Seite initialisieren, rechte Seite an Pivotisierung anpassen
    flops = flops + n*(n+(n-1));            % für jedes Element des neuen Vektors (n) werden n Multiplikationen und n-1 Additionen benötigt
    for i=1:n                               % alle Zeilen
        for j=1:i-1                         % alle Spalten links von i
            y(i) = y(i) - L(i,j)*y(j);      % Werte in y aktualisieren
            flops = flops + 2;              % eine Subtraktion, eine Multiplikation
        end
    end
end

function [x,flops] = rueckwaertssubstitution(U,y)

    flops = 0;
    [~,n] = size(U);                        % Größe von U 
    x = y;                                  % Ergebnisvektor mit rechter Seite initialisieren
    for i=n:-1:1                            % alle Zeilen beginnend mit der letzten
        for j=i+1:n                         % alle Spalten rechts von i
            x(i) = x(i) - U(i,j)*x(j);      % Werte in x aktualisieren
            flops = flops + 2;              % eine Subtraktion und eine Multiplikation
        end
        x(i) = x(i) / U(i,i);               % durch Diagonalelement teilen
        flops = flops + 1;                  % eine Division
    end
end
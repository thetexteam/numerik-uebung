% Hausaufgabe 2 - Schaltungsanalyse
% 
%
% Zun??chst soll das Gau??verfahren (LU-Zerlegung mit Pivotisierung, Vorw??rts- und R??ckw??rtssubstitution) implementiert und untersucht werden.
% Implementieren Sie in der MATLAB-Datei gaussverfahren.m die Funktion gaussverfahren(), welche das Gleichungssystem Ax=b mit A in R^(n x n) und b, x in R^n l??st.
% 
% Eingabewerte sind eine n x n Matrix A sowie ein Vektor f??r die rechte Seite b.
% R??ckgabewert ist der L??sungsvektor x.
% 
% a) Implementieren Sie die LU-Zerlegung mit Spaltenpivotisierung (= Zeilentausch) f??r eine quadratische Matrix in der Unterfunktion luzerlegung() und geben Sie den Rechenaufwand in Elementaroperationen
% (Addition, Subtraktion, Multiplikation oder Division von je zwei Skalaren) an:
% 
% Eingabewert ist eine n x n Matrix A.
% R??ckgabewerte sind die untere Dreiecksmatrix L, die obere Dreiecksmatrix U, die Permutationsmatrix P sowie die Anzahl der ben??tigten Flops flops.
% 
% b) Vergleichen Sie das Ergebnis und die Laufzeit Ihrer Implementierung der LU-Zerlegung mit der in MATLAB vorimplementierten Routine lu. 
% Wieviel Speicherplatz ben??tigen Sie f??r die Speicherung von L und U? Geben Sie eine Methode an, den Speicherplatz hierf??r auf maximal n^2 zu reduzieren, wenn keine Pivotisierung verwendet wird. 
% (Beantworten Sie die Frage in der MATLAB-Datei hausaufgabe2.m.)
% 
% c) Implementieren Sie die Vorw??rtssubstitution in der Unterfunktion vorw??rtssubstitution().
% 
% Eingabewerte sind eine untere Dreiecksmatrix L, die Permutationsmatrix P sowie ein Vektor b f??r die rechte Seite.
% R??ckgabewert ist der Vektor y sowie die Anzahl der ben??tigten Flops (= Elementaroperationen) flops.
% 
% d) Implementieren Sie die R??ckw??rtssubstitution in der Unterfunktion r??ckw??rtssubstitution().
% 
% Eingabewerte sind eine obere Dreiecksmatrix U sowie der Vektor y.
% R??ckgabewert ist der L??sungsvektor x sowie die Anzahl der ben??tigten Flops (= Elementaroperationen) flops.
% 
%
% Nun soll die Schaltung in Bild 1 analysiert und mit der oben implementierten Routine gel??st werden.
% Bearbeiten Sie die folgenden Aufgaben in der MATLAB-Datei hausaufgabe2.m.
% 
% e) Fertigen Sie ein Ersatzschaltbild f??r die gegebene Schaltung an, bei dem die Operationsverst??rker OP1 und OP2 durch spannungsgesteuerte Spannungsquellen wie in Bild 2 modelliert werden.
% Erg??nzen Sie die Schaltung hierzu um die Eingangswiderst??nde R1 bzw. R3 sowie um gesteuerte Spannungsquellen mit Innenwiderst??nden R2 bzw. R4 und Verst??rkungsfaktoren v_1 = v_2 = 10^4.
% Zeichnen Sie den gerichteten Graphen des Ersatzschaltbildes (Orientieren Sie sich an den vorgegebenen Kantenstr??men!), bestimmen Sie die Inzidenzmatrix A und geben Sie A aus.
% Beachten Sie, dass die zur externen Stromquelle geh??rige Kante nicht in die Inzidenzmatrix einflie??t.
% 
% f) Welche Dimension hat der Nullraum von A? was ist der Rang von A? Was bedeutet dies f??r die Schaltungsanalyse?
% 
% g) Stellen Sie - ohne Ber??cksichtigung der Quellen - das 1. Kirchhoffsche Gesetz (Knotenregel, KCL) mit Hilfe der Matrix A sowie des Vektors w der Zweigstr??me auf. In welchem Fundamentalen Unterraum liegt w und welche Dimension hat dieser? Was bedeutet dies anschaulich f??r den Graphen bzw. die Schaltung?
% 
% h) Stellen Sie - ohne Ber??cksichtigung der Quellen - das 2. Kirchhoffsche Gesetz (Maschenregel, KVL) mit Hilfe von A, des Vektors e der Kantenspannungen sowie des Vektors u der Knotenpotentiale auf. In welchem Fundamentalen Unterraum liegt e?
% 
% i) Betrachten Sie nun die Schaltung mit ihren Bauelementen und stellen Sie das Ohmsche Gesetz mit Hilfe von w, e sowie einer Leitwertsmatrix C auf. Geben Sie die Inverse von C, die Widerstandsmatrix C_inv, mit folgenden Baelementwerten aus:
% 
% R1 = R3 = 100k Ohm
% R2 = R4 = 100 Ohm
% R5 = 1k Ohm
% R6 = R7 = R9 = 10k Ohm
% R8 = 5k Ohm
% 
% j) Betrachten Sie die gesteuerten Spannungsquellen nun zun??chst als unabh??ngige Quellen und erweitern Sie die oben hergeleiteten Gleichungen f??r KCL und KVL um die Quellenvektoren b (Spannungsquellen) und f (Stromquellen). Geben Sie b und f f??r I_0=1mA und die Annahme u1=u3=0.1mV aus.
% 
% k) Fassen Sie die Zusammenh??nge aus g) - j) in einem Gleichungssystem Mx=r zusammen. Stellen Sie unter Verwendung von A und C eine Blockmatrix M auf und verwenden Sie die Vektoren w,u,b und f. Geben Sie die Blockmatrix M sowie die rechte Seite r aus.
% Welche Eigenschaften hat die Blockmatrix M? Ist sie invertierbar?
% 
% l) Betrachten Sie die Spannungsquellen nun als spannungsgesteuerte Quellen, die von den Knotenpotenzialen abh??ngen. Modifizieren Sie die Bl??cke A und b im Gleichungssystem aus k) entsprechend und geben Sie das ge??nderte Gleichungssystem (M_mod, r_mod) an. Welche Eigenschaft der Blockmatrix ist nun nicht mehr erf??llt?
% 
% m) W??hlen Sie nun Knoten 6 als Bezugsknoten und reduzieren Sie das Gleichungssystem, sodass es eindeutig l??sbar wird. Berechnen Sie die L??sung mit Ihrem implementierten Programm gaussverfahren(). Geben Sie die Werte f??r die Kantenstr??me w und die Knotenspannungen u aus.
% Kontrollieren Sie ihr Ergebnis durch Vergleich mit der \ (backslash)-Funktion in MATLAB.
% 
% n) Das Verhalten der Schaltung ist vom Wert I_0 abh??ngig. Welcher Aufwand ergibt sich bei der erneuten Berechnung von w und u unter Verwendung der bisherigen Ergebnisse, wenn sich nur I_0 ??ndert? Wie kann man von der LU-Zerlegung profitieren?
% 
% o) Gegeben seien eine n x n-Bandmatrix mit je k oberen und unteren Nebendiagonalen und eine n x n-Blockmatrix mit k x k-Bl??cken. Bestimmen Sie den Speicher- und Rechenaufwand einer LU-Zerlegung mit und ohne Spaltenpivotisierung f??r beide Matrizen.

function hausaufgabe2
display('Erkl??rung zu Aufgabe b): Insgesamt werden n?? Speicherpositionen ben??tigt.')
% insert code here

A = [-1  0  1  0  0  0;
      0  0  0 -1  0  1;
      0  0 -1  0  1  0;
      0 -1  0  0  0  1;
     -1  1  0  0  0  0;
      0 -1  1  0  0  0;
      0  0 -1  1  0  0;
      0  0  0 -1  1  0;
      0  0  0  0 -1  1]
%imagesc(A)

display('Erkl??rung zu Aufgabe f): Der Rang von A ist 5, Dimension des Nullraums N(A) = 1')
disp(rank(A))

display('g) Es gilt: (A^T)w = 0, w liegt im Nullraum von A^T')

display('h) Es gilt: Au = -e, e liegt im Spaltenraum von A') 

display('i) Es gilt: w = Ce mit')
C = diag([1e-5 1e-2 1e-5 1e-2 1e-3 1e-4 1e-4 0.2e3 1e4])
CInv = diag([1e5 1e2 1e5 1e2 1e3 1e4 1e4 5e3 1e4])

display('j) Es gilt : (A^T)w = f und b-Au = e')
f = zeros(6,1);
f(1,1) = -1e-3;
f(6,1) = 1e-3;
disp('f = ');
disp(f)

b = zeros(9,1);
b(2,1) = -1;
b(4,1) = 1;
disp('b = ');
disp(b)

display('k) A symmetrisch, nicht invertierbar');
M = [CInv A; A' zeros(6,6)]
r = [b;f]

display('l)');
v = 1e4;
V = zeros(9,6);
V(2,1) = -v;
V(2,3) = v;
V(4,3) = v;
V(4,5) = -v;
disp('V = ');
disp(V)

M_mod = [CInv, (A-V); A', zeros(6)]
r_mod = [zeros(9,1); f]

display('m)')
A_red = A(:,1:5);
V_red = V(:,1:5);
M_mod_red = [CInv, (A_red-V_red); A_red', zeros(5)]
f_red = f(1:5,:);
r_mod_red = [zeros(9,1); f_red]

x = gaussverfahren(M_mod_red, r_mod_red);

w = x(1:9)
u = x(10:end)

display('n) O(n^2) mit n = 14')
display('o) Bandstruktur, ohne Pivot: Speicher: n + 2kn - k(k+1) = O(nk), Rechenaufwand: (n-1)(k+1)k = O(nk^2)')
display('   Bandstruktur, mit Pivot: Speicher: n + 2kn - k(k+1) + k(n-k-1) = O(nk), Rechenaufwand: (n-1)(2k+1)k = O(nk^2)')
display('   Blockstruktur, ohne Pivot: Speicher: n*k = O(nk), Rechenaufwand: (n/3)*(k^2-1) = O(nk^2)')
display('   Blockstruktur, mit Pivot: Speicher: O(nk), Rechenaufwand: (n/3)*(k^2-1) = O(nk^2)')

end

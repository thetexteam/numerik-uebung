function [] = sparseMatrices()
    iterEnd = 1000;                                                             % largest size of A
    iterStart = 10;                                                             % smallest size of A
    step = 10;                                                                  % step size

    iter = 1;
    p = iterStart : step : iterEnd;
    size = ((iterStart-iterEnd)/step)+1;                                        % size of vectors
    memFull = zeros(size,1);                                                    % preallocation
    memSparse = zeros(size,1);
    memCOO = zeros(size,1);
    timeFull = zeros(size,1);
    timeSparse = zeros(size,1);
    timeCOO = zeros(size,1);
    for i=p
        m=i;                                                                    % number of rows of A
        n=i;                                                                    % number of columns of A
        b = ones(m,1);                                                          % vector to multiply with

        nonzeros = 0.1*m*n;                                                     % number of nonzeros in A
        [AFull, ASparse, row, col, val] = createSparseAndFull(m,n,nonzeros);    % create sparse matrix, full matrix and COO format
                                            
        memFull(iter) = getMemMatrix(AFull);                                    % get memory needed of full matrix
        memSparse(iter) = getMemMatrix(ASparse);                                % get memory needed of sparse matrix
        memCOO(iter) = getMemCOO(row, col, val);                                % get memory needed of COO format
        
        timeFull(iter) = multiplyMatrix(AFull,b);                               % get time needed to multiply full matrix with b
        timeSparse(iter) = multiplyMatrix(ASparse,b);                           % get time needed to multiply sparse matrix with b
        timeCOO(iter) = multiplyCOO(row,col,val,b);                             % get time needed to multiply matrix in COO format with b
        
        iter = iter+1;                                                          % start next iteration
    end
    
    hold on;                                                                    % plot memory needed 
    plot(p,memFull, '-bo');                         
    plot(p, memSparse, '-ro');
    plot(p, memCOO, '-go');
    xlabel('n (number of rows/cols in A)');
    ylabel('number of bytes');
    legend('full matrix', 'sparse matrix', 'COO format');
    title('Memory needed by matrix storage formats')
    hold off;
    
    figure                                                                      % plot time needed 
    plot(p,timeFull,'-bo');
    hold on;
    plot(p,timeSparse, '-ro');
    plot(p,timeCOO, '-go');
    xlabel('n (number of rows/cols in A)');
    ylabel('time');
    legend('full matrix', 'sparse matrix', 'COO format');
    title('Time needed for matrix-vector-multiplication')
    hold off;
    
    display('Erläuterungen zur Aufgabe 1: ');
    
end

function [AFull, ASparse, row, col, val] = createSparseAndFull(m,n, nonzeros)
    AFull = zeros(m,n);                                                         % all elements in A should be zero
    row = zeros(nonzeros,1);                                                    % define row numbers
    col = zeros(nonzeros,1);                                                    % define col numbers
    val = zeros(nonzeros,1);                                                    % define values
    i = 1;
    while i <= nonzeros                                                         % create nonzero elements
        r = randi(m,1);                                                         % randomly choose a row
        c = randi(n,1);                                                         % randomly choose a column
        if (AFull(r, c) == 0)                                                   % if this was not choosen before
            row(i) = r;                                                         % set row for COO format
            col(i) = c;                                                         % set col for COO format
            val(i) = rand(1);                                                   % randomly generate a value for that matrix enrtry
            AFull(r,c) = val(i);                                                % set matrix element in full matrix
            i = i+1;
        end;
    end
    
    ASparse = sparse(AFull);                                                    % generate sparse matrix
    
end

function [mem] = getMemMatrix(A)
     temp = whos('A');                                                          % get memory needed of matrix A
     mem = temp.bytes;
end

function [mem] = getMemCOO(row, col, val)
    temp =  whos('row');                                                        % get memory needed for row
    mem = temp.bytes;
    temp = whos('col');                                                         % get memory needed for col
    mem = mem + temp.bytes;
    temp =  whos('val');                                                        % get memory needed for val
    mem = mem + temp.bytes;
end

function [time] = multiplyMatrix(A,b)
    tic;                                                                        % start time measurement
    x = A*b;                                                                    % multiply A and b
    time = toc;                                                                 % end time measurement
end


function [time] = multiplyCOO(row, col, val, b)
    tic;                                                                        % start time measurement
    [n,~] = size(row);                                                          % determine number of nonzeros (= size of row)
    x = zeros(max(size(b)),1);                                                  % generate a result vector
    for i=1:n
       x(row(i)) = x(row(i)) + val(i)*b(col(i));                                % multiply matrix (A) and vector (b)
    end;
    time = toc;                                                                 % end time measurement
end
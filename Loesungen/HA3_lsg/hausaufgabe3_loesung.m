function [] = hausaufgabe3()

left = 0;                                       % linke Intervallgrenze
right = 1;                                      % rechte Intervallgrenze
n = 100;                                        % Anzahl der inneren Stützstellen
h = 1/(n+1);                                    % Schrittweite (äquidistante Stützstellen)
t = left+h:h:right-h;                           % alle inneren Stützstellen
[A,b] = finiteDifferenzen(left,right,n);        % stelle LGS auf
u = A\b;                                        % Löse Gleichungssystem 

plot(t,u,'-bo');                                % zeichne berechnete Lösung
hold on;
plot(t,-0.5*t.^2+0.5*t, '-rs')                  % zeichne exakte Lösung
xlabel('x');
ylabel('Lösung u(x)');
title('Loesung der DGL');
legend('Finite-Differenzen-Lösung', 'exakte Lösung');
hold off;

plotSOR;
plotPCG;

end

function [x,iter] = sor(A,b,x0,omega, TOL)
    D = diag(diag(A));                              % Zerlegung von A in A = D+L+U mit D DIagonalmatrix
    L = tril(A) - D;                                % L untere Dreiecksmatrix
    U = triu(A) - D;                                % U obere Dreiecksmatrix
    temp = (1-omega)*D-omega*U;                     
    B = D+omega*L;                                  % Iterationsmatrix
    ck = B\(omega*b);
    x = x0;
    iter = 0;
    r = b-A*x;                                      % Residuum
    while (norm(r) > TOL)
       x = B\(temp*x) + ck;                         % neue Iterierte
       iter = iter+1;
       r = b-A*x;                                   % Residuum zur neuen Lösung (für Abbruchkriterium)
    end
end

function [x,iter] = pcg(A,b,P,x0,TOL)
    x = x0;                                     % Startvektor
    r = b-A*x;                                  % Residuum zum Startvektor
    z = P\r;                                    % z = P^(-1) * A
    p = z;
    iter = 0;
    while norm(r) > TOL

       alpha = (p'*r)/(p'*A*p);                 % neue Schrittweite
       x = x+alpha*p;                           % neue Iterierte 
       r = r - alpha*A*p;                       % Residuum zum neuen Punkt
       z = P\r;                         
       beta = ((A*p)'*z) / ((A*p)'*p);          
       p = z-(beta*p);                          % neue Suchrichtung
       iter = iter+1;
    end
end



function [A,b] = finiteDifferenzen(left,right,n)
    h = 1/(n+1);                                        % Schrittweite
    t = left+h:h:right-h;                               % Stützstellen
    A = (1/h^2)*sparse(2*diag(ones(n,1)) + (-1)*diag(ones(n-1,1),-1) + (-1)*diag(ones(n-1,1),1));    % Finite-Differenzen-Matrix
    
    b = rightHandSide(t);                               % rechte Seite
end

function f = rightHandSide(x)
    f = ones(max(size(x)),1);
    
end

function plotSOR()
    figure(3);                                             % erstelle neues Bild
    left = 0;                                           % linke Intervallgrenze
    right = 1;                                          % rechte Intervallgrenze
    iterations = zeros(1,91);
    
    omega = [1, 1.5, 1.9];                              % verschiedene Relaxationsparameter
    for j = omega
        it = 1;
        for i=10:100                                    % variiere Anzahl der Stützstellen
            n = i;                                      % Anzahl der Stützstellen
            [A,b] = finiteDifferenzen(left,right,n);    % stelle Gleichungssystem auf
            x0 = zeros(n,1);                            % Startvektor für SOR-verfahren
            [~,iter] = sor(A,b,x0,j,1e-5);              % Löse lineares Gleichungssystem
            iterations(it) = iter;                      % Speichere Anzahl der Iterationen in Vektor ab
            it = it+1;
        end
        plot((10:100),(iterations));                    % zeichne Anzahl der Iterationen über Anzahl der Stützstellen
        hold all;
    end
    hold off;
    title('Lösung mit SOR');
    xlabel('Anzahl der Stützstellen n');
    ylabel('Anzahl der benötigten Iterationen');
    legend('omega = 1', 'omega = 1.5', 'omega = 1.9');
    
    disp('Erläuterung zum optimalen Relaxationsparameter: ')
end

function plotPCG()
    figure;                                                                                     % neues Bild 
    left = 0;                                                                                   % linke Intervallgrenze
    right = 1;                                                                                  % rechte Intervallgrenze
    iterations = zeros(1,91);
    time = zeros(1,91);
    
    for j=1:3                                                                                   % für 3 verschiedene Vorkonditionierer
        it = 1;
        for i=10:100                                                                            % variiere Anzahl der Stützstellen
            n = i;                                                                              % Anzahl der Stützstellen (10<=n<=100)
            [A,b] = finiteDifferenzen(left,right,n);                                            % stelle LGS auf
            x0 = zeros(n,1);                                                                    % Startvektor für CG-Verfahren
            if (j == 1)
                P = eye(n);                                                                     % ohne Vorkonditionierer
            elseif (j == 2)
                P = diag(diag(A));                                                              % Diagonalvorkonditionierer
                if (i==100)
                    display('Die Kondition der Matrix A ist:');
                    disp(condest(A));                                                           % Zeige Kondition von A
                    display('Die Kondition der Matrix inv(P)*A mit P = diag(A) ist:')
                    disp(condest(inv(P)*A));                                                    % Zeige Kondition von P^(-1) * A
                end;
            elseif (j == 3)              
                [L,U] = ilu(A);                                                                 % unvollständige LU-Zerlegung als Vorkonditionierer
                P = L*U;
                if (i==100)
                    display('Die Kondition der Matrix inv(P)*A mit P = ilu(A) ist:')
                    disp(condest(inv(P)*A));                                                    % Zeige Kondition von P^(-1) * A
                end;
            end;
            tic;                                                                                % Starte Zeitmessung
            [~,iter] = pcg(A,b,P,x0,1e-5);                                                      % Löse LGS mit PCG-verfahren
            time(it) = toc;                                                                     % Beende Zeitmessung
            iterations(it) = iter;                                                              % Speicher Anzahl der Iterationen in einem Vektor
            it = it+1;
        end;
        
        subplot(1,2,1);                                                                         % subplot: Anzahl der Iterationen über Anzahl der Stützstellen
        plot(10:100,iterations);
        hold all;
        subplot(1,2,2)                                                                          % subplot: Zeit über Anzahl der Stützstellen
        plot(10:100,time);
        hold all;
    end
    subplot(1,2,1);
    title('Lösung mit CG');
    xlabel('Anzahl der Stützstellen n');
    ylabel('Anzahl der benötigten Iterationen');
    legend('P=I','P=D','P=ilu');
    subplot(1,2,2);
    title('Lösung mit CG');
    xlabel('Anzahl der Stützstellen n');
    ylabel('benötigte Zeit');
    legend('P=I','P=D','P=ilu');   
    hold off;
    
    display('Erläuterung zu den Vorkonditionierern: ');
end
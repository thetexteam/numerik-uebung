function splines()
    
    xi = 0:12;                                              % Stützstellen
    [a,b,c,d]=splineParameter(xi,@(x)sin(x));               % Bestimme Spline-Parameter
    X = [];                                                 % plot-Punkte
    Y = [];                                                 % plot-Punkte
    for i=1:11                                              % berechne Splines
        t = xi(i):1e-1:xi(i+1);                             % Hilfspunkte zum Zeichnen
        XNew = t';
        YNew = zeros(max(size(t)),1);
        for j=1:max(size(t))  
            YNew(j) = splineVal(xi,t(j),a,b,c,d,i);
        end
        X = [X XNew];                                       % hänge neu berechnete Punkte an alte Punkte ran
        Y = [Y YNew];                                       % hänge neu berechnete Punkte an alte Punkte ran
    end
    hold on;
    plot(X,Y,'--b');
    plot(xi,sin(xi),'ro');
    hold off;
    
    plotsymbol();
end

function f = splineVal(xi,x, a, b, c, d, j)
    f = a(j)+b(j)*(x-xi(j))+c(j)*(x-xi(j))^2+d(j)*(x-xi(j))^3;  % Funktionswert an der Stelle x
end

function [a,b,c,d]=splineParameter(xi,fun)
    n = max(size(xi));                                  % Anzahl der Stützstellen
    a = fun(xi);                                        % Parameter a=f(x_i)
    h = zeros(n-1,1);                                   % Schrittweite

    for i=1:n-1
       h(i) = xi(i+1)-xi(i);
    end
    A = sparse(zeros(n,n));                             % Matrix für LGS zur Bestimmung der Parameter
    bs = zeros(n,1);                                    % rechte Seite für LGS zur Bestimmung der Parameter
    for i=2:n-1
       A(i,i) = 2*(h(i)+h(i-1));
       A(i,i-1) = h(i-1);
       A(i,i+1) = h(i);
       bs(i) = (3/h(i))*(a(i+1)-a(i)) - (3/h(i-1))*(a(i)-a(i-1));
    end
    A(1,1) = 1;
    A(n,n) = 1;
    c = A\bs;                                           % Lösung des LGS
    b = zeros(n,1);                                     % Parameter b für Splines
    d = zeros(n,1);                                     % Parameter d für Splines
    for i=1:n-1
        b(i) = (1/h(i))*(a(i+1)-a(i))-(h(i)/3)*(2*c(i)+c(i+1)); % Formel aus Vorlesung
        d(i) = (1/(3*h(i)))*(c(i+1)-c(i));                      % Formel aus Vorlesung
    end
end

function yi = symbol()
    yi = [1 2 3 6 9.5 13 15.5 16 15.5 13 12 15 18 19 18 16 13 17 19.5 20.75 19.5 16.5 14 13 15.5 18.5 19.5 18.5 15 12 9.5 9 9.25 11 11.5 10 6 3.5 2 0.5];
end

function xi = symbolX()
    xi = [3.5 4 4.5 4 3.5 2 1 1.5 2.5 4 5 4.5 4 4.5 5.5 6 7.5 8 8.25 9.25 10.25 10 9.75 10 11 12 13 13.5 13 12 11.5 12 13 14.5 17 16.5 13 10 9.5 9];
end

function plotsymbol()
   figure();
   xi = symbolX();                                                  % Stützstellen
   yi = symbol();                                                   % Funktionswerte an den Stützstellen
   n = max(size(xi));                                               % Anzahl der Stützstellen
   t = zeros(size(xi));                                             % Parameter
   for i=2:n
      t(i) = t(i-1) + sqrt((xi(i)-xi(i-1))^2+(yi(i)-yi(i-1))^2);    % Parameter = Länge des Pfades
   end
   [ax,bx,cx,dx]=splineParameter(t,@(t)symbolX());                  % Splins S_x durch (t,xi)
   [ay,by,cy,dy]=splineParameter(t,@(t)symbol());                   % Spline S_y durch (t,yi)
   
   X = [];                                                          % plot-Punkte
   Y = [];
   steps = 1e-1;                                                
 
   for j=1:n-1                                                      % für jedes S_j
       tn = t(j):steps:t(j+1);                                      % Zwischenpunkte
       newX = zeros(size(tn));                                      % plot Punkte für S_j
       newY = zeros(size(newX));                                    % plot Punkte für S_j
       for i=1:max(size(newX))
           newX(i) = splineVal(t, tn(i), ax,bx,cx,dx,j);
           newY(i) = splineVal(t, tn(i),ay,by,cy,dy,j);
       end
       X = [X newX];                                                % Hinzufügen der Punkte von S_j zu allen plot Punkten
       Y = [Y newY];
   end
   plot(X,Y,'--b')                                                  % plot Splines
   hold on
   plot(xi,yi,'ro');                                                % plot Stützpunkte
  
   hold off;
end
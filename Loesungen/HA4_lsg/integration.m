function integration()

% Start with testfunction y' = -sin(t)

% y(1) = 1;
% x(1) = 0;
% iter = 2;
% h = 1;
% 
% 
% 
% for i=h:h:40
%    % y(iter) = implizitEuler(i,y(iter-1),h,@sinus);
%    % y(iter) = trapez(i,i-1,y(iter-1),h,@sinus); 
%     x(iter) = i;
%     
%     iter=iter+1; 
% end
% plot(x,y);

planeten();             % Löse Drei-Körperproblem

end

% function f = sinus(t,x)
%     f=-sin(t);
% end

function y = explizitEuler(t,yold,h,fun)
    y = yold + h*fun(t,yold);                                       % ein Schritt des expliziten Eulerverfahrens
end

function y = heun(t,yold,h,fun)
    y  = yold + (h/2)*(fun(t,yold) + fun(t+h,yold+h*fun(t,yold)));  % ein Schritt des Heun-Verfahrens
end

function y = adamsbashforth(t,yold,h,fun,k)
    b = [[1 0 0 0];[3/2 -1/2 0 0];[23/12 -16/12 5/12 0];[55/24 -59/24 37/24 -9/24]];    % Butcher-Tableau für Adams-Bashfort
    
    y=reshape(yold(1,:,:),6,2);                                                         % aus 3D-array y ein 2D-Array y machen
    
    for i=1:k
       temp = reshape(yold(i,:,:),6,2); 
       y = y + h*b(k,i)*fun(t(i),temp);                                                 % ein Schritt Adams-Bashfort
    end
    
end

% function y = implizitEuler(t,yold,h,fun)                                    
%     x0 = yold;
%     y = fsolve(@nonlinearfun,x0,optimset('display', 'off'),yold,t,fun,h);               % ein Schritt des impliziten Euler-verfahrens
% end
% 
% 
% function f=nonlinearfun(y,yold,t,fun,h)
%     f=yold+feval(fun,t,y)*h-y;                                                          % nichtlineare Gleichung für impliziten Euler
% end

function y = trapez(t,told,yold,h,fun)
    x0 = yold;                                                                          % Startwert für Nullstellensuche
    y = fsolve(@trapezfun,x0,optimset('display', 'off'),yold,t,told,fun,h);             % ein Schritt im Trapezverfahren (löse nichtlineare Gleichung)
end

function f=trapezfun(y,yold,t,told,fun,h)
    f=yold+(feval(fun,t,y)+feval(fun,told,yold))*(h/2)-y;                               % nichtlineare Gleichung für Trapezverfahren
end


function f=rhs(~,y)                                                                     % rechte Seite der DGL des 3-Körper-Problems
    %m=[1; 9.55e-4; 1e-14]; % Sun, Jupiter, Halley
    m=[1; 3e-6; 1e-14];                                                                 % Masse von Sun, Earth, Halley
    f=zeros(6,2);                                                                       % Initialisierung
    
    f(1,:) = y(2,:);
    f(2,:) = (-m(2)/(norm(y(1,:)-y(3,:)))^3)*(y(1,:)-y(3,:)) + (-m(3)/(norm(y(1,:)-y(5,:)))^3)*(y(1,:)-y(5,:));
    f(3,:) = y(4,:);
    f(4,:) = (-m(1)/(norm(y(3,:)-y(1,:)))^3)*(y(3,:)-y(1,:)) + (-m(3)/(norm(y(3,:)-y(5,:)))^3)*(y(3,:)-y(5,:));
    f(5,:) = y(6,:);
    f(6,:) = (-m(1)/(norm(y(5,:)-y(1,:)))^3)*(y(5,:)-y(1,:)) + (-m(2)/(norm(y(5,:)-y(3,:)))^3)*(y(5,:)-y(3,:));
end

function planeten()     
    n = 1000;                                                                           % für t<=1000        
    h = [0.1 0.01 0.001];                                                               % verschiedene Schrittweiten
    
    %y = [[0 0];[0 0];[0 5.36];[-0.425 0];[34.75 0];[0 0.0296]]; % Sun, Jupiter, Halley
    y =  [[0 0];[0 0];[0 1];[-1 0];[34.75 0];[0 0.0296]];                               % Anfangswerte des Systems (Sun, Earth, Halley)
    
    figure()                                                                            % Berechnung und plot für expliziten Euler und h=0.1,0.01,0.001
    for i=1:3
        t = 0:h(i):n;
        subplot(3,1,i);
        planetenEulerHeun(@explizitEuler,t,y,h(i),@rhs);
        title('Eulerverfahren');
    end
    
    figure()                                                                            % Berechnung und Plot für Heun und h=0.1 und 0.01
    for i=1:2
        t = 0:h(i):n;
        subplot(2,1,i);
        planetenEulerHeun(@heun,t,y,h(i),@rhs);
        title('Heunverfahren');
    end
    
    figure()                                                                            % Berechnung und Plot für Adams-Bashforth und h=0.1 und 0.01
    for i=1:2
        t = 0:h(i):n;
        subplot(2,1,i);
        planetenAB(t,y,h(i),@rhs);
        title('Adams-Bashforth-Verfahren');
    end
    
    figure()                                                                            % Berechnung und Plot für Trapezverfahren und h=0.1
    t = 0:h(1):n;
    planetenTrapez(t,y,h(1),@rhs);
    title('Trapezverfahren für h=0.1');
    
    
end

function planetenEulerHeun(verfahren,t,y0,h,rhs)
    numpoints = max(size(t));                                                           % Anzahl der Zeitpunkte
    y=y0;                                                                               % Startwert
    p1 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 1
    p2 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 2
    p3 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 3
    p1(1,:) = y(1,:);
    p2(1,:) = y(3,:);
    p3(1,:) = y(5,:);
    
    for i=2:numpoints
       y = verfahren(t(i),y,h,rhs);                                                     % ein Integrationsschritt
       p1(i,:) = y(1,:);
       p2(i,:) = y(3,:);
       p3(i,:) = y(5,:);
    end
    hold on;
    plot(p1(:,1),p1(:,2),'-b');
    plot(p2(:,1),p2(:,2),'-r');
    plot(p3(:,1),p3(:,2),'-g');
end

function planetenAB(t,y0,h,rhs)
    numpoints = max(size(t));                                                           % Anzahl der Zeitpunkte
    y=y0;                                                                               % Startwert
    p1 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 1
    p2 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 2
    p3 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 3
    p1(1,:) = y(1,:);
    p2(1,:) = y(3,:);
    p3(1,:) = y(5,:);
    
    tOld = zeros(4,1);
    tOld(1) = t(1);
    yOld = zeros(4,6,2);
    yOld(1,:,:) = y;
    for i=2:numpoints
       y = adamsbashforth(tOld,yOld,h,rhs,min(4,i-1));                                  % ein Integrationsschritt
       
       yOld(2:4,:,:) = yOld(1:3,:,:);
       yOld(1,:,:) = y;
       tOld(2:4) = tOld(1:3);
       tOld(1) = t(i);
       
       
       p1(i,:) = y(1,:);
       p2(i,:) = y(3,:);
       p3(i,:) = y(5,:);
    end
    hold on;
    plot(p1(:,1),p1(:,2),'-b');
    plot(p2(:,1),p2(:,2),'-r');
    plot(p3(:,1),p3(:,2),'-g');
end

function planetenTrapez(t,y0,h,rhs)
    numpoints = max(size(t));                                                           % Anzahl der Zeitpunkte
    y=y0;                                                                               % Startwert
    p1 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 1
    p2 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 2
    p3 = zeros(numpoints,2);                                                            % Werte für Plots für Planet 3
    p1(1,:) = y(1,:);
    p2(1,:) = y(3,:);
    p3(1,:) = y(5,:);
    
    for i=2:numpoints
       y = trapez(t(i),t(i-1),y,h,rhs);                                                 % ein Integrationsschritt
       p1(i,:) = y(1,:);
       p2(i,:) = y(3,:);
       p3(i,:) = y(5,:);
    end
    hold on;
    plot(p1(:,1),p1(:,2),'-b');
    plot(p2(:,1),p2(:,2),'-r');
    plot(p3(:,1),p3(:,2),'-g');
end

function x = gaussverfahren(A, b)
[L, U, P, flopsZerlegung] = luzerlegung(A);
%display(L);
%display(U);
[y, flopsVorwaertsSubstitution] = vorwaertssubstitution(L, P, b);
[x, flopsRueckwaertsSubstitution] = rueckwaertssubstitution(U, y);
display(strcat('flops: ', num2str(flopsZerlegung + flopsVorwaertsSubstitution + flopsRueckwaertsSubstitution)));
end

function [L, U, P, flops] = luzerlegung(A)

n = size(A, 1);
L = zeros(n, n);
P = eye(n);
flops = 0;

for i = 1:n-1
    [pivot, pivotIndex] = max(abs(A(i:n, i)));
    pivotIndex = pivotIndex + (i - 1);
    pivot = A(pivotIndex, i);
    Psub = eye(n);
    Psub(:, [i, pivotIndex]) = Psub(:, [pivotIndex, i]);
    A([i, pivotIndex], :) = A([pivotIndex, i], :);
    L([i, pivotIndex], :) = L([pivotIndex, i], :);
    P = Psub*P;
    pivotRow = A(i, i+1:n);
    for j = i+1:n
        factor = A(j, i)/pivot;
        L(j, i) = factor;
        flops = flops + 1;
        
        currentRow = A(j, i+1:n);
        A(j, i+1:n) = currentRow - factor*pivotRow;
        flops = flops + 2*(n - i + 1);
        A(j, i) = 0;
    end
end

U = A;
L = L + eye(n);

end

function [y, flops] = vorwaertssubstitution(L, P, b)

n = size(L, 1);
y = zeros(n, 1);
b = P*b;
y(1) = b(1)/L(1, 1);
flops = 1;

for i = 2:n
    rowSum = L(i, 1:i-1)*y(1:i-1);
    flops = flops + (i-1) + (i-2);
    y(i) = (b(i) - rowSum)/L(i, i);
    flops = flops + 2;
end

end

function [x, flops] = rueckwaertssubstitution(U, y)

n = size(U, 1);
x = zeros(n, 1);
x(n) = y(n)/U(n, n);
flops = 1;

for i = n-1:-1:1
    rowSum = U(i, i+1:n)*x(i+1:n);
    flops = flops + (n-i) + (n-i-1);
    x(i) = (y(i) - rowSum)/U(i, i);
    flops = flops + 2;
end

end

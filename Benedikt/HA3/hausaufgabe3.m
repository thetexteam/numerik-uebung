function [] = hausaufgabe3()
    n = 10:10:100;
    close all;
    plotSOR(n);
    plotPCG(n);
    plotBackslashOperator(n);
    plotExactSolution();
end

function [x, iter] = sor(A, b, x0, omega, tolerance)
    D = diag(diag(A));
    R = -triu(A, 1);
    L = -tril(A, -1);
    M = 1/omega*D - L;
    N = (1/omega - 1)*D + R;
    
    iter = 0;
    epsilon = tolerance + 1;
    x = x0;
    while epsilon >= tolerance
        xNew = M\(N*x + b);
        epsilon = norm(xNew - x);
        iter = iter + 1;
        x = xNew;
    end
end

function [x, iter] = pcg(A, b, Pinvers, x0, tolerance)
    iter = 0;
    r = b - A*x0;
    z = Pinvers*r;
    p = z;
   
    epsilon = tolerance + 1;
    x = x0;
    while epsilon >= tolerance
        alpha = (p'*r)/(p'*A*p);
        xChange = alpha*p;        
        epsilon = norm(xChange);
        x = x + xChange;
        r = r - alpha*A*p;
        z = Pinvers*r;
        beta = ((A*p)'*z)/((A*p)'*p);
        p = z - beta*p;
        iter = iter + 1;
    end
end

function [A, b] = finiteDifferenzen(n)
    h = 1/(n + 1);
    b = (-h^2)*ones(n + 2, 1);
    b(1) = 0;
    b(n + 2) = 0;
    A = zeros(n + 2, n + 2);
    A(1, 1) = 1;
    A(n + 2, n + 2) = 1;
    
    for i = 2:n+1
        A(i, i-1) = 1;
        A(i, i) = -2;
        A(i, i+1) = 1;
    end
    
    A = sparse(A);
end

function plotSOR(n)
    figure;  
    omega = [1, 1.5, 1.9];  
    iterationsOmegaOne = zeros(length(n), 1);
    iterationsOmegaTwo = zeros(length(n), 1);
    iterationsOmegaThree = zeros(length(n), 1);
    
    subplot(2, 2, 1);
    hold on;    
    for i = 1:length(n)
        [A, b] = finiteDifferenzen(n(i));
        x0 = zeros(n(i) + 2, 1);
        [x, iterationsOmegaOne(i)] = sor(A, b, x0, omega(1), 1e-5);
        plotSolutionVector(x, n(i));
    end
    ylabel('u(x)');
    xlabel('x');
    title(strcat('omega=', num2str(omega(1))));
    hold off;
    
    subplot(2, 2, 2);
    hold on;    
    for i = 1:length(n)
        [A, b] = finiteDifferenzen(n(i));
        x0 = zeros(n(i) + 2, 1);
        [x, iterationsOmegaTwo(i)] = sor(A, b, x0, omega(2), 1e-5);
        plotSolutionVector(x, n(i));
    end
    ylabel('u(x)');
    xlabel('x');
    title(strcat('omega=', num2str(omega(2))));
    hold off;
    
    subplot(2, 2, 3);
    hold on;    
    for i = 1:length(n)
        [A, b] = finiteDifferenzen(n(i));
        x0 = zeros(n(i) + 2, 1);
        [x, iterationsOmegaThree(i)] = sor(A, b, x0, omega(3), 1e-5);
        plotSolutionVector(x, n(i));
    end
    ylabel('u(x)');
    xlabel('x');
    title(strcat('omega=', num2str(omega(3))));
    hold off;
    
    subplot(2, 2, 4);
    hold on;
    plot(n, iterationsOmegaOne, '-r+');
    plot(n, iterationsOmegaTwo, '-bo');
    plot(n, iterationsOmegaThree, '-gx');
    legend(strcat('omega=', num2str(omega(1))), strcat('omega=', num2str(omega(2))), strcat('omega=', num2str(omega(3))));
    ylabel('iterations');
    xlabel('n');
    hold off;
    
    set(gcf,'name','SOR','numbertitle','off');
    disp('Erlaeuterung zum optimalen Relaxationsparameter: ');
    disp('Die Berechnung des exakten Relaxationsparameters wuerde eine Berechnung von Eigenwerten beinhalten,');
    disp('was das Verfahren wieder langsamer machen wuerde als andere Alternativen. Bei diesem Beispiel scheint');
    disp('der optimale Werte eher bei 2 zu liegen');
end

function plotPCG(n)
    figure;
    iterationsWithout = zeros(length(n), 1);
    iterationsDiagonal = zeros(length(n), 1);
    iterationsIncomplete = zeros(length(n), 1);
    
    subplot(2, 2, 1);
    hold on;    
    for i = 1:length(n)
        [A, b] = finiteDifferenzen(n(i));
        x0 = zeros(n(i) + 2, 1);
        [x, iterationsWithout(i)] = pcg(A, b, eye(n(i) + 2), x0, 1e-5);
        plotSolutionVector(x, n(i));
        title('Ohne Vorkonditionierung');
    end
    ylabel('u(x)');
    xlabel('x');
    hold off;
    
    subplot(2, 2, 2);
    hold on;    
    for i = 1:length(n)
        [A, b] = finiteDifferenzen(n(i));
        x0 = zeros(n(i) + 2, 1);
        Pinvers = diag(1./diag(A));
        [x, iterationsDiagonal(i)] = pcg(A, b, Pinvers, x0, 1e-5);
        plotSolutionVector(x, n(i));
        title('Diagonalvorkonditionierer');
    end
    ylabel('u(x)');
    xlabel('x');
    hold off;
    
    subplot(2, 2, 3);
    hold on;    
    for i = 1:length(n)
        [A, b] = finiteDifferenzen(n(i));
        x0 = zeros(n(i) + 2, 1);
        [L, U] = ilu(A);
        Pinvers = inv(U)*inv(L);
        [x, iterationsIncomplete(i)] = pcg(A, b, Pinvers, x0, 1e-5);
        plotSolutionVector(x, n(i));
        title('Unvollstaendige LU-Zerlegung als Vorkonditionierer');
    end
    ylabel('u(x)');
    xlabel('x');
    hold off;
    
    subplot(2, 2, 4);
    hold on;
    plot(n, iterationsWithout, '-r+');
    plot(n, iterationsDiagonal, '-bo');
    plot(n, iterationsIncomplete, '-gx');
    legend('Ohne', 'Diagonal', 'Unvollstaendig');
    ylabel('iterations');
    xlabel('n');
    hold off;
    
    set(gcf,'name','PCG','numbertitle','off');
    display('Erlaeuterung zu den Vorkonditionierern: ');
    display('Eine Diagonal-Vorkonditionierung bringt bei diesem Beispiel keinen Vorteil gegenueber der Variante');
    display('ohne Vorkonditionierung. Die unvollstaendige LU-Zerlegung hingegen fuehrt zu einer Konvergenz innerhalb');
    display('von nur 2 Iterationen.');
end

function plotBackslashOperator(n)
    figure;
    hold on;
    
    for i = 1:length(n)
        [A, b] = finiteDifferenzen(n(i));
        x = A\b;
        plotSolutionVector(x, n(i));
    end
    hold off;
    
    set(gcf,'name','backslash operator','numbertitle','off');
end

function plotSolutionVector(x, n)
    h = 1/(n + 1);
    plot(0:h:1, x);
end

function plotExactSolution()
    figure;
    left = 0;
    right = 1;
    f = @(x) x*(1 - x)/2;
    fplot(f, [left, right]);
    set(gcf,'name','exact solution','numbertitle','off');
end
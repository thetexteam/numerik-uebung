function [] = sparseMatrices()
    iterEnd = 500;                                   
    iterStart = 10;                                                            
    step = 10;                                                               
    repeatTimeMeasurement = 1000;
    
    p = iterStart : step : iterEnd;
    count = size(p, 1);
    memFull = zeros(count, 1);
    memSparse = zeros(count, 1);
    memCOO = zeros(count, 1);
    timeFull = zeros(count, 1);
    timeSparse = zeros(count, 1);
    timeCOO = zeros(count, 1);
    
    rng('default');
    rng(1);
    
    for i = p
        % create matrices and determine time & memory needed
        [AFull, ASparse, row, col, val] = createSparseAndFull(i, i, i*i*0.1);
        index = int64(i/step);
        memFull(index) = getMemMatrix(AFull);
        memSparse(index) = getMemMatrix(ASparse);
        memCOO(index) = getMemCOO(row, col, val);
        b = ones(i, 1);
        
        [timeFullSum, xFull] = multiplyMatrix(AFull, b);
        for j = 2:repeatTimeMeasurement
            timeFullSum = timeFullSum + multiplyMatrix(AFull, b);
        end
        timeFull(index) = timeFullSum/repeatTimeMeasurement;
        
        [timeSparseSum, xSparse] = multiplyMatrix(AFull, b);
        for j = 2:repeatTimeMeasurement
            timeSparseSum = timeSparseSum + multiplyMatrix(ASparse, b);
        end
        timeSparse(index) = timeSparseSum/repeatTimeMeasurement;
        
        [timeCOOSum, xCOO] = multiplyMatrix(AFull, b);
        for j = 2:repeatTimeMeasurement
            timeCOOSum = timeCOOSum + multiplyCOO(row, col, val, b);
        end
        timeCOO(index) = timeCOOSum/repeatTimeMeasurement;   
        
        if norm(xFull - xSparse) > 0.0001
            display('full and sparse multiplication delivered different results');
        end
        
        if norm(xFull - xCOO) > 0.0001
            display('full and COO multiplication delivered different results');
        end                                                  
    end

    subplot(1, 2, 1);
    semilogy(p, memFull, 'Color', 'red');
    ylabel('bytes');
    xlabel('n');
    hold on;
    semilogy(p, memSparse, 'Color', 'blue');
    semilogy(p, memCOO, 'Color', 'green');
    legend('full', 'sparse', 'COO');
    hold off;
    subplot(1, 2, 2);
    semilogy(p, timeFull, 'Color', 'red');
    ylabel('seconds');
    xlabel('n');
    hold on;
    semilogy(p, timeSparse, 'Color', 'blue');
    semilogy(p, timeCOO, 'Color', 'green');
    legend('full', 'sparse', 'COO');
    hold off;

    display('Erlaeuterungen zur Aufgabe 1: ');
    display('Die Speicherung einer duennbesetzen Matrix ist, vom Speicherverbrauch her gesehen,');
    display('erheblich effizienter, wenn nur die Positionen und Werte gespeichert werden. Die interne');
    display('Implementierung in Matlab ist sogar noch einmal ein wenig besser als eine haendische Variante.');
    display('Diese Art der Speicherung einer Matrix wirkt sich jedoch nachteilig bei Rechenoperationen');
    display('wie zum Beispiel bei einer Multiplikation aus');
end

function [AFull, ASparse, row, col, val] = createSparseAndFull(m, n, nonzeros)
    positions = randperm(m*n, nonzeros);
    row = zeros(1, nonzeros);
    col = zeros(1, nonzeros);
    val = zeros(1, nonzeros);
    AFull = zeros(m, n);

    for i = 1:nonzeros
        position = positions(i);
        positionRow = int64(floor(position/n)) + 1;
        positionColumn = int64(position - (positionRow - 1)*n) + 1;
        value = rand();
        row(i) = positionRow;
        col(i) = positionColumn;
        val(i) = value;
        AFull(positionRow, positionColumn) = value;
    end

    if nonzeros ~= nnz(AFull)
        display('the number of nonzero elements is wrong');
    end
    
    ASparse = sparse(AFull);
end

function [mem] = getMemMatrix(A)
     var = whos('A');
     mem = var.bytes;
end

function [mem] = getMemCOO(row, col, val)
   varRow = whos('row');
   varCol = whos('col');
   varVal = whos('val');
   mem = varRow.bytes + varCol.bytes + varVal.bytes;
end

function [time, x] = multiplyMatrix(A, b)
    tic;
    x = A*b;
    time = toc;
end


function [time, x] = multiplyCOO(row, col, val, b)
    tic;
    
    n = size(b, 1);
    x = zeros(n);
    for i = 1:length(row)
        currentRow = row(i);
        currentCol = col(i);
        currentVal = val(i);
        x(currentRow) = x(currentRow) + currentVal*b(currentCol);
    end
    
    time = toc;
end
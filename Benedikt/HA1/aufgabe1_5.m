% Aufgabe 5: Power-Iteration
%
% Die Power-Iteration-Methode zur numerischen Bestimmung des größten Eigenwerts und des dazugehörigen Eigenvektors einer Matrix M ist wie folgt definiert:
%
% x := Mx
% x := x/||x||
% bis x konvergiert
%
% Wenn M genau einen betragsmäßig höchsten Eigenwert besitzt, dann konvergiert x zu einem Vielfachen des korrespondierenden Eigenvektors.
%
%
% ---------------------------------------------------------------------------
% Aufgabenteil I
% 
% In diesem Aufgabenteil soll die Methode verstanden und veranschaulicht werden.
% 
% Gegeben ist die Matrix A= [0.4 -0.6 0.2; -0.3 0.7 -0.4; -0.1 -0.4 0.5]
%
% a) Welchen Rang hat die Matrix A? Was bedeutet dies für die Eigenwerte?
% Berechnen Sie die Eigenwerte und die dazugehörigen Eigenvektoren mit der eig-Funktion in MATLAB. Welches ist der größte Eigenwert lambda_max und zu 
% welchem Eigenvektor v_max gehört er?
%
% Implementieren Sie die oben vorgestellte Methode in der vorgegebenen Funktion showPowerIteration():
% Eingabeparameter: Matrix 'M', Startvektor 'x', Anzahl der Iterationen 'N'
%
% Berechnen Sie für jede Iteration den geschätzten größten Eigenwert von A.
% Ermitteln Sie in jedem Iterationsschritt die Differenz vom normierten 'x' zum tatsächlichen 'v_max'. Als Fehlermaß dient der Euklidsche Abstand.
% Stellen Sie diese Differenz sowie den ermittelten größten Eigenwert für n=1,2...N graphisch nebeneinander (subplot) dar. Skalieren Sie den Betrag
% der Differenz logarithmisch (log10), wählen Sie eine geeignete Achsenskalierung und beschriften Sie die Zeichnung vollständig.
%
% Vervollständigen Sie das Hauptprogramm:
%
% b) Rufen Sie die Funktion im Hauptprogramm für M=A, N=50 und 100 zufällig generierte x auf. 
% Überlagern Sie die Darstellung für alle x und beschreiben Sie das Ergebnis.
%
% c) Rufen Sie die Funktion im Hauptprogramm für M=A, N=10 und x=v_max auf. 
% Was geschieht im Gegensatz zu b) und warum?
%
% d) Rufen Sie die Funktion im Hauptprogramm für M=A, N=10 und x=v_2 auf, dem Eigenvektor zum betragsmäßig zweitgrößten Eigenwert. 
% Was geschieht im Gegensatz zu b) und c) und warum?
%
% e) Rufen Sie die Funktion im Hauptprogramm für M=A, N=50 und x= v_min = 1/sqrt(3) * [1;1;1], den dritten Eigenvektor, auf. 
% Warum geschieht hier etwas unterschiedliches als in d)?
%
% ---------------------------------------------------------------------------
% Aufgabenteil II
%
% In diesem Aufgabenteil soll die Rechenzeit der Power-Iteration-Methode untersucht werden.
%
% Hierzu ist eine Modifikation der bisher implementierten Funktion nötig. Um die Funktionalität von Aufgabenteil I nicht zu beeinträchtigen,
% implementieren Sie für Aufgabenteil II die Funktion measurePowerIteration():
% Eingabeparameter: Wie showPowerIteration(), außerdem Abbruchparameter 'epsilon'
% Rückgabewert: Rechenzeit 'time'
%
% Berechnen Sie für jede Iteration den geschätzten größten Eigenwert von M.
% Ermitteln Sie in jedem Iterationsschritt die Differenz vom normierten 'x' zum tatsächlichen 'v_max'.
% Brechen Sie die Methode ab, sobald diese Differenz kleiner als 'epsilon' wird.
% Bestimmen Sie die benötigte Rechenzeit sowie die Differenz für alle durchlaufenen n und stellen Sie den Zusammenhang graphisch dar. Skalieren Sie die
% Differenz logarithmisch (log10) und beschriften Sie die Zeichnung vollständig.
%
% f) Bestimmen Sie, wie lange die eig-Funktion für die Bestimmung von v_max von A benötigt und speichern Sie den Wert in 'eig_time_A'.
% Rufen Sie nun die Funktion showPowerIteration() für 'A', einen zufälligen Vektor 'x', N=1000 sowie epsilon = 10^-12 auf. Speichern Sie die benötigte 
% Rechenzeit in 'pi_time_A', vergleichen und kommentieren Sie die Ergebnisse.
%
% g) Die Matrix B sei eine quadratische Zufallsmatrix der Dimension 1000. Berechnen Sie wie in f) die Rechenzeiten für beide Methoden und
% speichern Sie die Ergebnisse in 'eig_time_B' sowie 'pi_time_B'. Was beobachten Sie?
%
% h) Nennen Sie einige mögliche Nachteile bzw. Schwachstellen der Power-Iteration.

function aufgabe1_5

close all

% Aufgabenteil I
A= [0.4 -0.6 0.2; -0.3 0.7 -0.4; -0.1 -0.4 0.5];
lambda_max=0;
v_max = zeros(3,1);
v_2 = zeros(3,1);
v_min = 1/sqrt(3)*[1;1;1];

% a)
display('Erlaeuterung zu den Eigenschaften von A:')

display(strcat('Rang von A: ',num2str(rank(A))));
display(strcat('damit sind alle Eigenwerte von A ungleich 0'));
[V, D] = eig(A);
[unused, permutation] = sort(diag(D));
D = D(permutation, permutation);
V = V(:, permutation);
lambda_max = D(3, 3);
v_max = V(:, 3);
lambda_2 = D(2, 2);
v_2 = V(:, 2);
lambda_min = D(1, 1);
v_min = V(:, 1);

display(lambda_max)
display(v_max)
display(lambda_min)
display(v_min)
display(lambda_2)
display(v_2)

% b)
N=50;

figure
subplot(1,2,1)
hold on
subplot(1,2,2)
hold on

for i=1:100
    showPowerIteration(A,rand(3,1),N,v_max);
end

display('Erlaeuterung zu b):')
display('Die berechneten Eigenwerte sind immer korrekt, nur die dazu passenden Eigenvektoren manchmal nicht. Warum? Gute Frage ...')

% c)
N=10;

figure

showPowerIteration(A,v_max,N,v_max);

display('Erlaeuterung zu c):')
display('Aufgrund der beschr�nkten Rechengenauigkeit wird der berechnete Eigenvektor schlechter ausgehend von dem eigentlich bereits (im Sinne der Rechengenauigkeit) korrekten Eigenvektor')

% d)
N=10;

figure

showPowerIteration(A,v_2,N,v_max);

display('Erlaeuterung zu d):')
display('Der Fehler w�rde an und f�r sich kleiner werden, nur ist das Verhalten diesbez�glich sehr langsam')

% e)
N=50;

figure

showPowerIteration(A,v_min,N,v_max);

display('Erlaeuterung zu e):')
display('In diesem FAll konvergiert das Verfahren zum richtigen Eigenwert, allerdings mit einem falschen Eigenvektor')


% Aufgabenteil II
N=1000;
epsilon=10^-12;

% f)

figure
x = rand(3, 1);

tic; eig(A); eig_time_A = toc;
tic; measurePowerIteration(A,x,N,v_max,epsilon); pi_time_A = toc;

display(eig_time_A)
display(pi_time_A)
display('Erlaeuterung zu f):')
display('Fuer kleine Matrizen ist eig erheblich schneller als die Power-Iteration')

% g)

figure
x = rand(1000, 1);
B = rand(1000, 1000);

[V, D] = eig(B);
[unused, permutation] = sort(diag(D));
D = D(permutation, permutation);
V = V(:, permutation);
v_max = V(:, 1000);

tic; eig(B); eig_time_B = toc;
tic; measurePowerIteration(B,x,N,v_max,epsilon); pi_time_B = toc;

display(eig_time_B)
display(pi_time_B)
display('Erlaeuterung zu g):')
display('Bei gro�en Matrizen wird die Power-Iteration schneller als eig.')

% h)
display('Nachteile der Power-Iteration:')
display('Es ist nicht sicher, dass sie ausreichend schnell zum richtigen Wert konvergiert. Zudem ist sie relativ langsam f�r kleine Dimensionen')


end


function showPowerIteration(M,x,N,v_max)

errors = zeros(N, 1);
eigenValues = zeros(N, 1);
for i = 1:N
    x = M*x;
    x = x/norm(x);
    errors(i) = norm(x/norm(x) - v_max);
    eigenValues(i) = x'*M*x;
end

subplot(1, 2, 1);
plot(1:N, log10(errors));
title('error of eigenvector');
xlabel('n');
ylabel('log10(error)');
subplot(1, 2, 2);
plot(1:N, eigenValues);
title('eigenvalues');
xlabel('n');
ylabel('lambda');

end


function time = measurePowerIteration(M,x,N,v_max,epsilon)
time = 0;

errors = zeros(N, 1);
eigenValues = zeros(N, 1);
executionTimes = zeros(N, 1);
lastError = epsilon + 1;
i = 1;
startTime = tic;
while (lastError > epsilon) && (i <= N)
    x = M*x;
    x = x/norm(x);
    errors(i) = norm(x/norm(x) - v_max);
    eigenValues(i) = x'*M*x;
    executionTimes(i) = toc(startTime);  
    i = i + 1;  
end

Nreal = i - 1;

plot(executionTimes(1:Nreal), log10(errors(1:Nreal)));
title('error of eigenvector');
xlabel('execution time');
ylabel('log10(error)');

end

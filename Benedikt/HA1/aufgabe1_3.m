% Aufgabe 3: Maschinengenauigkeit am Beispiel der Zahl e
% 
% Die Zahl e ist durch lim n->inf (1+1/n)^n definiert. In dieser Aufgabe sollen Sie die Approximation von e untersuchen.
% 
% Implementieren Sie das Hauptprogramm:
% Approximieren Sie e für die 200 vorgegebenen Werte 'n' zwischen 10 und 10^20, jeweils in single sowie double precision.
% Berechnen Sie für beide die Abweichung zwischen dem Ergebnis, das MATLAB liefert und dem approximiertem Wert und speichern Sie den Fehler in den vorgegebenen Feldern
% 'err_single' bzw. 'err_double'.
% Stellen Sie den Verlauf der Abweichung über n in einer Zeichnung dar (verschiedene Farben! Beschriftung!) und kommentieren Sie die Ergebnisse.
%
% Nützliche Befehle: exp, plot, hold


function aufgabe1_3

N = 191;
err_single = zeros(N,1);
err_double = zeros(N,1);
x = zeros(N, 1);

for i = 1:N
    n = 10^((i + 9)/10);
    
    err_single(i, 1) = abs(calculateError(single(n)));
    err_double(i, 1) = abs(calculateError(double(n)));
    x(i, 1) = n;
    
end

loglog(x, err_single, 'b', x, err_double, 'g');
plotLegend = legend('single precision','double precision');
xlabel('n')
ylabel('error')

disp('Erlaeuterung:')
disp('Mathematisch gesehen erreicht man mit einem groesseren n ein exakteres Ergebnis.');
disp('Auf einer realen Maschine ausgefuehrt erreicht jedoch der Wert 1/n mit zunehmendem');
disp('n dermassen kleine Regionen, dass irgendwann bei der Addition mit 1 das Ergebnis');
disp('wiederum 1 ist. Bei hoeherer Genauigkeit wird dieser Punkt spaeter erreicht.');

end

function result = calculateE(n)
    result = (1 + 1/n)^n;
end

function error = calculateError(n)
    error = calculateE(n) - exp(1);
end

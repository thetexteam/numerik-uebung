% Aufgabe 4: Die Reihenfolge von Operationen kann das Ergebnis beeinflussen
%
% In der Numerik ist es manchmal vorteilhaft, eine Formel in einen mathematisch äquivalenten Ausdruck umzuformen.
% Dies soll an folgendem Beispiel demonstriert werden.
%
% Die Summe über k=1,2...K von k^-2 konvergiert für K->inf zu pi²/6.
% 
% Berechnen Sie die Summe mit einfacher Genauigkeit (single precision) bis K=10^8. Brechen Sie die Schleife ab, wenn sich die Summe nicht mehr
% verändert und geben Sie das k zu diesem Zeitpunkt aus.
% Speichern Sie die Abweichung zu pi²/6 in 'err_forward'.
%
% Führen Sie nun die Summation in umgekehrter Reihenfolge (k=K,K-1,...1) durch und speichern Sie die Abweichung zu pi²/6 in 'err_backward'.
%
% Vergleichen und begründen Sie die Ergebnisse.

function aufgabe1_4
K = 10^8;

disp('direct implementation');
sumForward = single(0);
for k = 1:K
    summand = 1/single(k^2);
    previousSum = sumForward;
    sumForward = sumForward + summand;
    if (sumForward == previousSum)
        disp('sum did not change anymore');
        break;
    end
end

disp('reverse implementation');
sumBackward = single(0);
for k = 1:K
    summand = 1/single((K + 1 - k)^2);
    sumBackward = sumBackward + summand;
end

correctValue = pi()^2/6;
err_forward = abs(sumForward - correctValue);
err_backward = abs(sumBackward - correctValue);

display('error forward:');
display(err_forward);
display('error backward:');
display(err_backward);
disp('Erlaeuterung:')
disp('Wenn man die Summation von hinten beginnt summiert man Werte in aehnlichen');
disp('Groessenordnungen. In der direkten Implementierung hingegen werden nach bereits');
disp('relativ wenigen Summanden Werte kleiner als das Maschinenepsilon addiert.');

end
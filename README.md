# Numerische Methoden der Elektrotechnik - Übung

Homework of the [EDA's](https://www.eda.ei.tum.de) course [Numerische Methoden der Elektrotechnik](https://www.eda.ei.tum.de/en/courses/lectures/numerische-methoden-der-elektrotechnik/).

Written by:

-   Markus Hofbauer
-   Kevin Meyer
-   Benedikt Schmidt

Check out the associated [script](https://github.com/MaKeAppDev/Skript-NMdE).
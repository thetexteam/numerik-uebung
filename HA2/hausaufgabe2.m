% Hausaufgabe 2 - Schaltungsanalyse
% 
%
% Zunächst soll das Gaußverfahren (LU-Zerlegung mit Pivotisierung, Vorwärts- und Rückwärtssubstitution) implementiert und untersucht werden.
% Implementieren Sie in der MATLAB-Datei gaussverfahren.m die Funktion gaussverfahren(), welche das Gleichungssystem Ax=b mit A in R^(n x n) und b, x in R^n löst.
% 
% Eingabewerte sind eine n x n Matrix A sowie ein Vektor für die rechte Seite b.
% Rückgabewert ist der Lösungsvektor x.
% 
% a) Implementieren Sie die LU-Zerlegung mit Spaltenpivotisierung (= Zeilentausch) für eine quadratische Matrix in der Unterfunktion luzerlegung() und geben Sie den Rechenaufwand in Elementaroperationen
% (Addition, Subtraktion, Multiplikation oder Division von je zwei Skalaren) an:
% 
% Eingabewert ist eine n x n Matrix A.
% Rückgabewerte sind die untere Dreiecksmatrix L, die obere Dreiecksmatrix U, die Permutationsmatrix P sowie die Anzahl der benötigten Flops flops.
% 
% b) Vergleichen Sie das Ergebnis und die Laufzeit Ihrer Implementierung der LU-Zerlegung mit der in MATLAB vorimplementierten Routine lu. 
% Wieviel Speicherplatz benötigen Sie für die Speicherung von L und U? Geben Sie eine Methode an, den Speicherplatz hierfür auf maximal n^2 zu reduzieren, wenn keine Pivotisierung verwendet wird. 
% (Beantworten Sie die Frage in der MATLAB-Datei hausaufgabe2.m.)
% 
% c) Implementieren Sie die Vorwärtssubstitution in der Unterfunktion vorwärtssubstitution().
% 
% Eingabewerte sind eine untere Dreiecksmatrix L, die Permutationsmatrix P sowie ein Vektor b für die rechte Seite.
% Rückgabewert ist der Vektor y sowie die Anzahl der benötigten Flops (= Elementaroperationen) flops.
% 
% d) Implementieren Sie die Rückwärtssubstitution in der Unterfunktion rückwärtssubstitution().
% 
% Eingabewerte sind eine obere Dreiecksmatrix U sowie der Vektor y.
% Rückgabewert ist der Lösungsvektor x sowie die Anzahl der benötigten Flops (= Elementaroperationen) flops.
% 
%
% Nun soll die Schaltung in Bild 1 analysiert und mit der oben implementierten Routine gelöst werden.
% Bearbeiten Sie die folgenden Aufgaben in der MATLAB-Datei hausaufgabe2.m.
% 
% e) Fertigen Sie ein Ersatzschaltbild für die gegebene Schaltung an, bei dem die Operationsverstärker OP1 und OP2 durch spannungsgesteuerte Spannungsquellen wie in Bild 2 modelliert werden.
% Ergänzen Sie die Schaltung hierzu um die Eingangswiderstände R1 bzw. R3 sowie um gesteuerte Spannungsquellen mit Innenwiderständen R2 bzw. R4 und Verstärkungsfaktoren v_1 = v_2 = 10^4.
% Zeichnen Sie den gerichteten Graphen des Ersatzschaltbildes (Orientieren Sie sich an den vorgegebenen Kantenströmen!), bestimmen Sie die Inzidenzmatrix A und geben Sie A aus.
% Beachten Sie, dass die zur externen Stromquelle gehörige Kante nicht in die Inzidenzmatrix einfließt.
% 
% f) Welche Dimension hat der Nullraum von A? was ist der Rang von A? Was bedeutet dies für die Schaltungsanalyse?
% 
% g) Stellen Sie - ohne Berücksichtigung der Quellen - das 1. Kirchhoffsche Gesetz (Knotenregel, KCL) mit Hilfe der Matrix A sowie des Vektors w der Zweigströme auf. In welchem Fundamentalen Unterraum liegt w und welche Dimension hat dieser? Was bedeutet dies anschaulich für den Graphen bzw. die Schaltung?
% 
% h) Stellen Sie - ohne Berücksichtigung der Quellen - das 2. Kirchhoffsche Gesetz (Maschenregel, KVL) mit Hilfe von A, des Vektors e der Kantenspannungen sowie des Vektors u der Knotenpotentiale auf. In welchem Fundamentalen Unterraum liegt e?
% 
% i) Betrachten Sie nun die Schaltung mit ihren Bauelementen und stellen Sie das Ohmsche Gesetz mit Hilfe von w, e sowie einer Leitwertsmatrix C auf. Geben Sie die Inverse von C, die Widerstandsmatrix C_inv, mit folgenden Baelementwerten aus:
% 
% R1 = R3 = 100k Ohm
% R2 = R4 = 100 Ohm
% R5 = 1k Ohm
% R6 = R7 = R9 = 10k Ohm
% R8 = 5k Ohm
% 
% j) Betrachten Sie die gesteuerten Spannungsquellen nun zunächst als unabhängige Quellen und erweitern Sie die oben hergeleiteten Gleichungen für KCL und KVL um die Quellenvektoren b (Spannungsquellen) und f (Stromquellen). Geben Sie b und f für I_0=1mA und die Annahme u1=u3=0.1mV aus.
% 
% k) Fassen Sie die Zusammenhänge aus g) - j) in einem Gleichungssystem Mx=r zusammen. Stellen Sie unter Verwendung von A und C eine Blockmatrix M auf und verwenden Sie die Vektoren w,u,b und f. Geben Sie die Blockmatrix M sowie die rechte Seite r aus.
% Welche Eigenschaften hat die Blockmatrix M? Ist sie invertierbar?
% 
% l) Betrachten Sie die Spannungsquellen nun als spannungsgesteuerte Quellen, die von den Knotenpotenzialen abhängen. Modifizieren Sie die Blöcke A und b im Gleichungssystem aus k) entsprechend und geben Sie das geänderte Gleichungssystem (M_mod, r_mod) an. Welche Eigenschaft der Blockmatrix ist nun nicht mehr erfüllt?
% 
% m) Wählen Sie nun Knoten 6 als Bezugsknoten und reduzieren Sie das Gleichungssystem, sodass es eindeutig lösbar wird. Berechnen Sie die Lösung mit Ihrem implementierten Programm gaussverfahren(). Geben Sie die Werte für die Kantenströme w und die Knotenspannungen u aus.
% Kontrollieren Sie ihr Ergebnis durch Vergleich mit der \ (backslash)-Funktion in MATLAB.
% 
% n) Das Verhalten der Schaltung ist vom Wert I_0 abhängig. Welcher Aufwand ergibt sich bei der erneuten Berechnung von w und u unter Verwendung der bisherigen Ergebnisse, wenn sich nur I_0 ändert? Wie kann man von der LU-Zerlegung profitieren?
% 
% o) Gegeben seien eine n x n-Bandmatrix mit je k oberen und unteren Nebendiagonalen und eine n x n-Blockmatrix mit k x k-Blöcken. Bestimmen Sie den Speicher- und Rechenaufwand einer LU-Zerlegung mit und ohne Spaltenpivotisierung für beide Matrizen.

function hausaufgabe2
disp(' ')
display('Erkl�rung zu Aufgabe b):')
display('F�r L und U ben�tig man jeweils n^2, also 2*n^2 Speicherplatz.')
display('Speichert man L und U wieder in A ben�tigt man nur n^2.')
disp(' ')

A = [-1     0     1     0     0     0
      0     0     0    -1     0     1
      0     0    -1     0     1     0
      0    -1     0     0     0     1
     -1     1     0     0     0     0
      0    -1     1     0     0     0
      0     0    -1     1     0     0
      0     0     0    -1     1     0
      0     0     0     0    -1     1];
  
display('Erkl�rung zu Aufgabe f):')
display(['Dimension des Nullraums: ',num2str(size(A,1)-rank(A))])
display(['Rang von A: ',num2str(rank(A))])


end

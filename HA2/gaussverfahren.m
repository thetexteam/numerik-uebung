function x = gaussverfahren(A,b)

% Aufgabe b)
tic;
[L,U,P,flopsLU] = luzerlegung(A); 
time = toc;
display(['Eigene LU-Zerlegung: ',num2str(time),' s'])

tic;
lu(A);
time = toc;
display(['Matlab LU-Zerlegung: ',num2str(time),' s'])

[y,flopsV] = vorwaertssubstitution(L,P,b);

[x,flopsR] = rueckwaertssubstitution(U,y);

flops = flopsLU + flopsV + flopsR;
display(['flops gesamt: ', num2str(flops)])

end

function [L,U,P,flops] = luzerlegung(A)

n = size(A,1);
if n ~= size(A,2)
    error('A muss eine qudratische Matrix sein!!!')
end

P = eye(n);

for k=1:n-1
    [~, location] = max(abs(A(k:end,k)));
    location = location + k - 1;
    P([k,location],:) = P([location,k],:);
    A([k,location],:) = A([location,k],:);   
    for i=k+1:n
       A(i,k) = A(i,k)/A(k,k);
       A(i,k+1:n) = A(i,k+1:n) - A(i,k)*A(k,k+1:n);
    end
end

L = eye(n) + tril(A,-1); 
U = triu(A);

flops = 2/3*n^3 - 1/2*n^2 - 1/6*n;

end

function [y,flops] = vorwaertssubstitution(L,P,b)

n = size(A,1);
if n ~= size(A,2)
    error('A muss eine qudratische Matrix sein!!!')
end

for i=1:n
   b(i) = b(i) - L(i,1:i-1)*b(1:i-1); 
end

y = P*b;
flops = n^2 - n;

end

function [x,flops] = rueckwaertssubstitution(U,y)

n = size(A,1);
if n ~= size(A,2)
    error('A muss eine qudratische Matrix sein!!!')
end

for i=n:-1:1
   y(i) = y(i) - U(i,i+1:n)*y(i+1:n);
   y(i) = y(i) / U(i,i);
end

x = y;
flops = n^2;

end

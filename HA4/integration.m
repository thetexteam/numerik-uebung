function integration()

planeten();             % Löse Drei-Körperproblem

end

function y = explizitEuler(t,yold,h,fun)

y = yold + h*fun(t);

end

function y = heun(t,yold,h,fun)

y = explizitEuler(t,yold,h,fun);
y = 0.5*yold + 0.5*(y + h*fun(t+h));
   
end

function y = adamsbashforth(t,yold,h,fun,k)
assert(k <= 4);
b = [[1 0 0 0];[3/2 -1/2 0 0];[23/12 -16/12 5/12 0];[55/24 -59/24 37/24 -9/24]];    % Butcher-Tableau für Adams-Bashfort

if k == 1
    y = yold + h*sum(b(1,:).*fun((0:3)*h+t));
else
    y = adamsbashforth(t,yold,h,fun,k-1) + h*sum(b(k,:).*fun((0:3)*h+t));
end

end

function y = trapez(t,told,yold,h,fun)
    
y = yold + (t-told)/2 * (fun(t) + fun(t+h));

end

function f=rhs(~,y)
m1 = 1;
m2 = 3e-6;
m3 = 1e-14;

f = [y(2);
    -m2/norm(y(1)-y(3))^3 * (y(1)-y(3)) + -m3/norm(y(1)-y(5))^3 * (y(1)-y(5));
    y(4);
    -m1/norm(y(3)-y(1))^3 * (y(3)-y(1)) + -m3/norm(y(3)-y(5))^3 * (y(3)-y(5));
    y(6);
    -m1/norm(y(5)-y(1))^3 * (y(5)-y(1)) + -m2/norm(y(5)-y(3))^3 * (y(5)-y(3))];
end

function planeten()     
   
h = 0.1;
tvec = (0:h:1000);
yeuler = zeros(1,length(tvec));
yheun = zeros(1,length(tvec));
yadams = zeros(1,length(tvec));
ytrapez = zeros(1,length(tvec));

for t=0:h:1000
    
end

figure;
subplot(2,2,1);
plot(tvec,yeuler)
subplot(2,2,2);
plot(tvec,yheun)
subplot(2,2,3);
plot(tvec,yadams)
subplot(2,2,4);
plot(tvec,ytrapez)

end

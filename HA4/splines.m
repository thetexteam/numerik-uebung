function splines()

%% Aufgabe c
    xi = (0:12);
    f = @(x) sin(x);
    [a,b,c,d] = splineParameter(xi,f);
    S = zeros(1,length(xi));
    for i=1:length(xi)
        S(i) = splineVal(xi,xi(i),a,b,c,d,i);
    end
    figure;
    hold on;
    plot(xi,S);
    plot(xi,f(xi),'rx');

%% Aufgabe d
    plotsymbol();
end

function f = splineVal(xi,x, a, b, c, d, j)
   S = a + b.*(x-xi) + c.*(x-xi).^2 + d.*(x-xi).^3;
   f = S(j);
end

function [a,b,c,d]=splineParameter(xi,fun)
   n = length(xi);
   b = zeros(1,n);
   d = zeros(1,n);
   a = fun(xi);
   h = a(2:end);
   % berechne c
   A = zeros(n);
   bHilf = zeros(n,1);
   A(1,1) = 1;
   A(n,n) = 1;
   bHilf(1) = 0;
   bHilf(n) = 0;
   for i=2:n-1
       A(i,i-1) = h(i-1);
       A(i,i) = 2*(h(i-1)+h(i));
       A(i,i+1) = h(i);
       bHilf(i) = 3/h(i)*(a(i+1)-a(i)) - 3/h(i-1)*(a(i)-a(i-1));
   end
   c = A\bHilf;
   c = c';
   % berechne b und d
   for i=1:n-1
        b(i) = 1/h(i)*(a(i+1)-a(i)) - h(i)/3*(2*c(i)+c(i+1));
        d(i) = 1/(3*h(i))*(c(i+1)-c(i));
   end
end

function yi = symbol()
    yi = [1 2 3 6 9.5 13 15.5 16 15.5 13 12 15 18 19 18 16 13 17 19.5 20.75 19.5 16.5 14 13 15.5 18.5 19.5 18.5 15 12 9.5 9 9.25 11 11.5 10 6 3.5 2 0.5];
end

function xi = symbolX()
    xi = [3.5 4 4.5 4 3.5 2 1 1.5 2.5 4 5 4.5 4 4.5 5.5 6 7.5 8 8.25 9.25 10.25 10 9.75 10 11 12 13 13.5 13 12 11.5 12 13 14.5 17 16.5 13 10 9.5 9];
end

function plotsymbol()
figure();

x = symbolX();
y = symbol();
t = zeros(1,length(x));
position = 1:length(x);

for i=2:length(t)
    t(i) = norm([x(i),y(i)] - [x(i-1),y(i-1)]) + t(i-1);
end

Sx = zeros(1,length(x));
Sy = zeros(1,length(y));
[ax,bx,cx,dx] = splineParameter(position,x);
[ay,by,cy,dy] = splineParameter(position,y);

for i=1:length(x)
        Sx(i) = splineVal(t,x(i),ax,bx,cx,dx,i);
        Sy(i) = splineVal(t,y(i),ay,by,cy,dy,i);
end

plot(Sx,Sy);

end